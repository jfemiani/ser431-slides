\documentclass{beamer}
%\documentclass[handout]{beamer}
%\usetheme{Singapore}
\usetheme{Warsaw}

%\setbeamertemplate{footline}[frame number]
\newcommand*\oldmacro{}%
\let\oldmacro\insertshorttitle%
\renewcommand*\insertshorttitle{%
	\oldmacro\hfill%
	\insertframenumber\,/\,\inserttotalframenumber}


\setbeamercovered{transparent}

\usepackage[procnames]{listings}
\usepackage{color}
\usepackage{clrscode}
\usepackage{cancel}

\title{Visual Appearance (RTR-5)}
\subtitle{SER431 - Lecture Notes}
\author{John C. Femiani}

\AtBeginLecture{\frame{\Large Today’s Lecture: \insertlecture}}



\begin{document}
    
\definecolor{keywords}{RGB}{255,0,90}
\definecolor{comments}{RGB}{0,0,113}
\definecolor{red}{RGB}{160,0,0}
\definecolor{green}{RGB}{0,150,0}
\lstset{language=Python, 
    basicstyle=\ttfamily\small, 
    keywordstyle=\color{keywords},
    commentstyle=\color{comments},
    stringstyle=\color{red},
    showstringspaces=false,
    identifierstyle=\color{green},
    procnamekeys={def,class},
    numbers=left,
    showspaces=false}
   
   
   \begin{frame}
   	\titlepage
   	
   \end{frame}
   
   \begin{frame} 
   	\frametitle{Outline}
   	\tableofcontents
   \end{frame}
   
\lecture{week 2, lecture 1}{W2L1}   
\section{Refresher on Lighting}
\subsection{Visual Phenomenon -- 5.1}
   
\begin{frame}
\frametitle{Refresher on Lighting}
\begin{center}
\includegraphics[width=0.7\linewidth]{RTRFigures/RTR3_05_01.png}
\end{center}
\end{frame}

\begin{frame}
	\frametitle{Visual Phenominon}
	
		\begin{center}
			\includegraphics[width=0.5\linewidth]{illum1}
		\end{center}
		
	\begin{itemize}
		\item Light is emitted by the sun or other sources (natural or artificial)
		\item Light interacts with object in the scene, part is absorbed and part is scattered and propagated in new directions
		\item Finally, light is absorbed by a sensor (human eye, electronic sensor, or film)
	\end{itemize}


\end{frame}

\subsection{Light Sources -- 5.2}

\begin{frame}
	\frametitle{Light sources}
	\begin{itemize}
		\item \alert{Light sources} emit light, rather than scattering or absorbing it. 
		\item There are many representations of light sources (e.g. area lights vs point lights vs IES lobes)
	\end{itemize}
	
	\begin{center}
\includegraphics[width=0.7\linewidth]{LEAP-Phx}  
\raisebox{0.5\height}{\includegraphics[width=0.3\linewidth]{IES_Lobe}}
\end{center}

\end{frame}
\begin{frame}
	\frametitle{Light sources}
	\begin{itemize}
		\item Distant sources (sun) are simplest, called \alert{directional lights} 
		\item For rendering, directions are \alert{light vectors} $\mathbf{l}$, in world space.
		\item For directional lights, $\mathbf{l}$ is a unit vector (length of 1).
		\item For implementation reasons, convention is $\mathbf{l}$ points \alert{towards} the light source and \alert{opposite} the ray. 
	\end{itemize}
	\begin{center}
\includegraphics[width=0.7\linewidth]{RTRFigures/RTR3_05_02}
\end{center}
\end{frame}

\begin{frame}
	\begin{itemize}
	\item \alert{Irradiance} is the power through a unit area of surface, in photons per unit per second.
	\item Light can be colored; we represent values as arrays (vectors) of one value per filter ($R, G, B$).
	\item Irradiance values can be arbitrarily large
	\item Irradiance includes photons coming from many directions. 
	\end{itemize}
	
	\begin{center}
\includegraphics[width=0.7\linewidth]{RTRFigures/RTR3_05_05}
\end{center}

\end{frame}

\begin{frame}
	\begin{itemize}
		\item Can think of irradiance as density of photons.
		\item Irradiance depends on the angle of incidence $\Theta_i$ between the surface and the light source. 
		\item When the surface is not perpendicular to $\mathbf{l}$, the density increases proportional to $\cos \Theta$. 
	\end{itemize}
	
	\begin{center}
\includegraphics[width=0.7\linewidth]{RTRFigures/RTR3_05_04}
\end{center}

Conveniently, $\cos\Theta_i = \mathbf{l}\cdot \mathbf{n}$, where $\mathbf{n}$ is a unit vector perpendicular to the surface.  (It is a good thing $\mathbf{l}$ points opposite the direction of the ray)

\end{frame}

\def\ocos{\ensuremath{\overline{\cos}}}
\begin{frame}
	\begin{itemize}
		\item When rendering, if the light comes from behind the surface  ($\cos \Theta < 0$) then the surface is in shadow. 
		\item Let $E_L$ be the amount of energy through a surface perpendicular to the light direction, then
		\begin{align*}
		E &= E_L \ocos\Theta_i \\
		  &= E_L \max(\mathbf{n}\cdot \mathbf{l}, 0).
		\end{align*}
		\item The cumulative of multiple light sources are added together $$ E = \sum_{k=1}^{n} E_{L_k} \ocos \Theta_{i_k}$$ where $E_{L_k}$ and $\Theta_{i_k}$ are the values of $E_L$ and $\Theta_i$ for the $k$th directional light source. 
	\end{itemize}
\end{frame}


\subsection{Material}
\begin{frame}
\frametitle{Material}

\begin{itemize}
	\item Appearance is affected by microscopic details of a surface.
	\item Each \alert{material} is associated with a set of shader programs, textures, and other properties.
	\item Fundamental light-matter interactions are \alert{scattering} and \alert{absorbtion}. 
	\item \alert{scattering} is the light we see -- \alert{reflection} and \alert{refraction}. 
\end{itemize}
\begin{center}
\includegraphics[width=0.6\linewidth]{RTRFigures/RTR3_05_06}
\end{center}

\end{frame}
 
\begin{frame}
	\begin{itemize}
		\item Light that passes through a surface is \alert{refrected} or \alert{transmitted}.
		\item For opaque objects, light may be transmitted through the surface but ultimately is \alert{reflected} or \alert{absorbed}. 
		\item Many objects are partially opaque -- light scatters within the surface and is exits at different locations. 
	\end{itemize}
	\begin{center}
\includegraphics[width=0.7\linewidth]{RTRFigures/RTR3_05_07}
\includegraphics[width=0.3\linewidth]{Fig16-04}
\end{center}

\end{frame} 


\begin{frame}

	\begin{center}
		\includegraphics[width=0.7\linewidth]{RTRFigures/RTR3_05_07}
		\includegraphics[width=0.3\linewidth]{04fig01}
	\end{center}
	
	\begin{itemize}
		\item Smooth surfaces reflect light like billiards balls,\\
		 called \alert{specular} light.
		\item Micro-structures \alert{diffuse} the light (spread a portion of it out in multiple directions)
		\item \alert{Ambient} light from the environment hits shadows -- e.g. outdoors, shadows are (dark) sky blue!
		\item Each type of lighting is affected differently by microtructures. 
	\end{itemize}
	
\end{frame} 

\begin{frame}
	Our goal it to characterize the behavior of a material using a \alert{shading equation} that relates the incoming and outgoing directions of light to the surface color.   
	\begin{itemize}
		\item The equation will have a \alert{specular term} and a \alert{diffuse term} to model the to aspects of a material's interaction with light.
		\item The incoming light is the \alert{irradiance} of a surface.
		\item The outgoing light is the \alert{exitance}, using the symbol, $M$. 
		\item The \alert{ratio} between the \alert{exitance} / \alert{irradiance} is the \alert{surface color}, $\mathbf{c}$. 
	\end{itemize}
\end{frame}

\def\cspec{\ensuremath{\mathbf{c}_{\text{spec}}}}
\def\cdiff{\ensuremath{\mathbf{c}_{\text{diff}}}}
\begin{frame}
     Our shading equation will have two seperate terms that \alert{approximate} the complex interaction of light with some (plastic or metal) surfaces:
	\begin{itemize}
		\item The \alert{specular color} ($\cspec$) is the brighter, directional reflected highlight.  
		\item The \alert{diffuse color} ($\cdiff$) is the color most associated with an object, that is reflected equally in all directions. 
		\item The specular and diffuse components of a material depend on the details of its composition
	\end{itemize}
	
	\begin{center}
\includegraphics[width=1.0\linewidth]{amb_diff_spec}
%https://clara.io/learn/user-guide/lighting_shading/materials/material_types/webgl_materials
\end{center}

\end{frame}

\begin{frame}
	\begin{itemize}
		\item The size and nature of reflected lights depends on the surface microstructure.
		\item A smooth surface has a tighter beam of reflected light (smaller and brighter specular highlights)
	\end{itemize}

	\begin{center}
\includegraphics[width=0.7\linewidth]{RTRFigures/RTR3_05_08}
	\end{center}

\end{frame}

\begin{frame}
	Even though the precise appearance is a function of many structures, we choose an appropriate (statistical) model for light that depends on the scale of the object.
	\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{RTRFigures/RTR3_05_09}

\end{figure}

\end{frame}

\subsection{Sensors}
\begin{frame}
	Ultimately, we only know about light that reaches a sensor. 
	\begin{itemize}
		\item An \alert{imaging} sensor is actually an array of many small sensors.
		\item Sensors turn the incoming irradiance into a signal,
		\\ but they add the incoming energy over all directions, 
		\\ they are not \alert{directionally specific}.
		\item Imaging systems use an \alert{aperture} (opening) to restrict the directions of incoming light (focus). 
		\item The density of light flow \emph{in a specific direction} is \alert{radiance}, $L$. 
	\end{itemize}
	\begin{figure}
\centering
\includegraphics[width=0.4\linewidth]{RTRFigures/RTR3_05_10}

\end{figure}

\end{frame}


\begin{frame}
	\begin{itemize}
		\item In real imaging systems, each sensor still averages over multiple rays. 
		\item Sensors are covered by \alert{filters} the limit sensitivity to $RGB$ (like human cone cells)
		\item In idealized computer systems we often imagine a single ray. (a source of aliasing artifacts)
		\item The rays focus on a shared point $\mathbf{p}$. The direction of the ray is the \alert{view vector}, $\mathbf{v}$. 
	\end{itemize}
	
	\begin{figure}
\centering
\includegraphics[width=0.4\linewidth]{RTRFigures/RTR3_05_11}
\includegraphics[width=0.4\linewidth]{RTRFigures/RTR3_05_12}
\caption{}
\label{fig:RTR3_05_11}
\end{figure}

\end{frame}


\begin{frame}
	\begin{itemize}
		\item The relationship between a sensor and the image that is perceived / displayed is complex and nonlinear. 
		\item Techniques like \alert{gamma correction} and \alert{tone mapping} will address some of this.
		\item The perceived colors even depend on time -- e.g. the American flag illusion. 
	\end{itemize}
	\begin{center}
	\includegraphics[width=0.5\textwidth]{usa-flag}
	\end{center}
\end{frame}

\section{Shading}
\subsection{The lighting equation}

\begin{frame}
	Shading:
	\begin{itemize}
		\item Compute the \alert{outgoing radiance}, $L_o$ in direction $\mathbf{v}$ based on material properties and light sources.
		\item Many possible equations (many approximate complex, subtle, but significant phenomena)
	\end{itemize} 
	
	\alert{Lambertian shading:} The \alert{diffuse exitance} (not radiance) is
	$$ M_\text{diff} =\cdiff \otimes E_L \ocos \Theta_i $$
	where $\otimes$ is an elementwise product (for $R, G, B$ separately)
	\\
	$$L_\text{diff} = \frac{M_\text{diff}}{\pi} = \frac{\cdiff}{\pi} \otimes E_L \ocos \Theta_i $$
	the \alert{radiance} is divided by the range of $\Theta_i$ (a half-circle). 
	Usually we fold the $1/\pi$ into $E_L$. 
\end{frame}

\begin{frame}
	\begin{itemize}
		\item The \alert{specular exitence} is directionally specific (depends on $\mathbf{v}$ as well as $\Theta_i$)
		\item The exitance (sum over directions) is similar to diffuse term.
		$$ M_\text{spec} = \cspec\otimes E_L \ocos \Theta_i$$
		but the reflections are not evenly distributed over $[0,\pi]$.
		\item When the geometry and light source are kept fixed, but the view moves, the specular highlight appears to move across the surface. 
	\end{itemize}
	\begin{center}
\includegraphics[width=0.3\linewidth]{"2015-09-01 11_31_31-Blender"}
\includegraphics[width=0.3\linewidth]{"2015-09-01 11_31_58-Blender"}
\includegraphics[width=0.3\linewidth]{"2015-09-01 11_32_09-Blender"}
\end{center}

\end{frame}


\lecture{week 2, lecture 2}{W2L2}


\begin{frame}
	\begin{itemize}
		\item Since two directions are involved, this is a BRDF (Bidirectional Radiance Distribution Function)
		\item For numerical reasons we use a \alert{halfway vector} $\mathbf{h}$ rather than calculating the reflection of $\mathbf{l}$ around the normal. 
		
		$$\mathbf{h} = \frac{\mathbf{l} + \mathbf{v}}{\|\mathbf{l} + \mathbf{v}\|}$$
		
		\item When light reflects straight at your eye, $\mathbf{h}\cdot \mathbf{n}=1$ is maximal.
	\end{itemize}
	
		\begin{center}
			\includegraphics[width=0.4\linewidth]{RTRFigures/RTR3_05_13}
		\end{center}

\end{frame}

\begin{frame}{Specularity}
	Why use a halfway vector?
	\begin{itemize}
		\item Surfaces can be modeled using \alert{microfacts}, perfect mirrors with slightly different orientations.
		\item In this view, the \alert{normal} vector is a mode of a distribution of microfacets. 
		\item You \emph{only} receive the reflected photon when the microfacet normal is exactly halfway between the light vector and the view vector. 
	\end{itemize}
	\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{RTRFigures/RTR3_07_30}
\end{figure}

\end{frame}


\begin{frame}
	\begin{itemize}
			\item Let $m$ be a parameter that describes the smoothness of the surface (chosen ad hoc)
			\item Since the total energy is constant, the size and brightness of the highlight changes with $m$ (bottom row)
	\end{itemize}
	\begin{figure}
\centering
\includegraphics[width=.8\linewidth]{RTRFigures/RTR3_07_38}
\end{figure}

\end{frame}


\begin{frame}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\begin{itemize}
				
				\item The \alert{probability} distribution of microfacet angles (relative to the normal vector) determines the size of the highlight.
				
				\item For ad-hoc reasons, we imagine the distribution to be $\propto \ocos^m\Theta_h$ for some $m$.
				
				\item In order to normalize the PDF we evaluate over an entire hemisphere; see the \href{http://www.farbrausch.de/~fg/stuff/phong.pdf}{\beamerbutton{normalizing factor derived}}
			\end{itemize}
		\end{column}
		\begin{column}{0.5\textwidth}
			\includegraphics[width=1.0\textwidth]{RTRFigures/RTR3_07_37}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\begin{itemize}
		\item One formula (normalized BRDF):
			\begin{align*}
				L_\text{spec} &= \frac{m+8}{8\pi} \ocos^m \Theta_h M_\text{spec} \\
				&=  \frac{m+8}{8\pi} \ocos^m \Theta_h \cspec \otimes E_L\ocos \Theta_i	
			\end{align*} 	
			Remember that dot products can be used for $\ocos \Theta_h$ and $\ocos \Theta_i$. 
		\item Where do the 8's come from?
		\\ They approximate the normalization constants so that we dont reflect more light than we receive over the whole hemisphere \href{http://www.farbrausch.de/~fg/stuff/phong.pdf}{\beamerbutton{derived here}}
	\end{itemize}
\end{frame}


\begin{frame}{The total outgoing radiance}
	Putting it together, we can calculate the total outgoing radiance in direction $\mathbf{v}$
	\begin{align*}
		L_o(\mathbf{v}) &= L_\text{diff} + L_{spec}\\
			&= \left(\frac{\cdiff}{\pi} + \frac{m+8}{8\pi} \ocos ^m\Theta_h \cspec \right) \otimes E_L\ocos\Theta_i
	\end{align*}
	where $\mathbf{\ocos\Theta_h} = \max(0, \mathbf{h}\cdot\mathbf{n})$.
	\vspace{0.5cm}\\
	Historically, we used the \alert{Blinn-Phong} formula
		$$ L_o(\mathbf{v}) = \left(\ocos\Theta_i\cdiff + \ocos^m \Theta_h \cspec\right)\otimes B_L$$
	with quantity $B_L$ that was determined by ad-hoc (artistic) methods to produce the desired appearance.  
\end{frame}

\begin{frame} [shrink=10]{The shading equation}
	\framesubtitle{What to take away}
	\begin{itemize}
		\item The effects of multiple light sources are \alert{added} together.
		\item \alert{Radiance} is a density of photons per unit per angle, \alert{Irradiance} is per unit irrespective of angle (and both are per second)
		\item The amount of energy depends on the angles between a light source, the surface, and a viewer.
		\item Light can be divided into \alert{diffuse} and \alert{specular} terms that are each fractions of the \alert{irradiance} 
		\item The specular highlight is view-dependant, diffuse shading is not.
		\item A probability distribution of microfacets determines the specular highlight - the distribution is approximated by $\ocos\Theta_h$, normalized over a hemisphere. 
	\end{itemize}
\end{frame}

\begin{frame}{Implementing shading equations}
	\begin{itemize}
		\item Shading is usually implemented in hardware or as shaders that run on the GPU
		\item It is useful to cnsider the \alert{frequency of evaluation} for some quantities:
		\begin{itemize}
			\item \alert{Per-modal} computations are constant over the model 
			\item \alert{Per-primitve} computations are constant over the primitive (e.g. triangle) 
			\item \alert{Per-vertex} computations are constant over the vertex but can be linearly interpolated across the vertices.
			\item \alert{Per-fragment} computations happen once per pixel. 
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{Implementing shading equations}
	\begin{itemize}
		\item The view vector, $\mathbf{v}$, can br calculated from a surface position $\mathbf{p}$ and the viewer's positin $\mathbf{p}_v$ (often 0!).
		$$\mathbf{v} = \frac{\mathbf{p}_v - \mathbf{p}}{\| \mathbf{p}_v - \mathbf{p}\|}$$
		It varies very quickly!
		
		\item Material properties derived from $c_\text{diff}$, $\cspec$, and $m$ vary slowly
			\begin{align*}
			L_o(\mathbf{v}) &= \left(\underbrace{\frac{\cdiff}{\pi}}_{K_\text{diff}} + \underbrace{\frac{m+8}{8\pi}  \cspec}_{K_\text{spec}} \ocos ^m\Theta_h \right) \otimes E_L\ocos\Theta_i
			\end{align*}
		we introduce constants $K_\text{diff}$ and $K_\text{spec}$ to hold pre-normalized material properties. 
	\end{itemize}

\end{frame}

%\begin{frame}[fragile]{Implementing}
%	\begin{itemize}
%		\item Let's try this out ourselves.
%		\item I am going to render something using the python programming langauge (with the distribution from \href{http://continuum.io/downloads}{\beamerbutton{Anaconda}), because in my opinion it is easiest to install on various platforms}
%		\item My examples use a bash shell (windows users can install \href{http://gitscm.com}{\beamerbutton{git}}, which comes with a bash shell)
%		\item If you are a windows user, I recommend you also install \href{https://conemu.github.io/}{\beamerbutton{conemu}} so you have a functional terminal emulator to use with the git bash shell.
%		\item You should all have a favorite editor by now - if not I recommend a lightweight text editor like  \href{https://notepad-plus-plus.org/}{\beamerbutton{notepad++ (windows)}} in addition to a heavier IDE like \href{https://www.jetbrains.com/pycharm/}{\beamerbutton{PyCharm}} etc.
%	\end{itemize}
%\end{frame}
%
%
%\begin{frame}[fragile]{Implementing}
%	\begin{exampleblock}{Setup using bash (e.g. git bash)}
%		\begin{lstlisting}[language=Bash]
%conda create -n ser431 ipython numpy 
%activate ser431
%pip install pyglet numpy-stl
%		\end{lstlisting}
%	\end{exampleblock}
%	\begin{itemize}
%		\item[Line 1] creates a \emph{virtual environment}, which allows  me to control which libraries or packages my project will use. I included \alert{numpy} for working with matrices an the \alert{ipython} shell.
%		\item[Line 2] switches from the global environment to the one I created.
%		\item[Line 3] uses a package manager (pip) to install \href{https://bitbucket.org/pyglet/pyglet/wiki/Home}{\beamerbutton{pyglet}}, a simple graphics library, and \href{https://github.com/WoLpH/numpy-stl}{\beamerbutton{numpy-stl}} for reading 3D mesh files.
%	\end{itemize}
%\end{frame}
%
%\begin{frame}{Implemententing}
%	\framesubtitle{Getting 3D meshes}
%
%	\begin{itemize}
%		\item I want to render something interesting, so I will be using the numpy-stl package to open a file.
%		\item I used \href{http://meshlab.sourceforge.net/}{\beamerbutton{MeshLab}}, which is a useful think to have around, in order to convert a mesh to the STL format.
%		\item Meshlab comes with sample meshes, you can find others online at places like \href{http://segeval.cs.princeton.edu/}{\beamerbutton{the mesh segmentation banchmark}}
%	\end{itemize}	
%	\begin{figure}
%\centering
%\includegraphics[width=0.4\linewidth]{Meshlab-knot}
%
%\end{figure}
%
%\end{frame}
%
%
%
%\begin{frame}[fragile]{Implementing}
%	Type 'ipython' to start an interactive python session. 
%	
%	\begin{exampleblock}{ipython}
%		\begin{lstlisting}[numbers=none]
%>>> from stl.mesh import Mesh
%>>> knot = Mesh.from_file('path/to/knot.stl')
%>>> len(knot)  # The number of faces
%2880
%
%>>> print knot.vectors[0] # Points on a triangle
%[[  4.51268387  28.68865967 -76.55680847]
% [  7.50471115  31.48498535 -79.55582428]
% [ 10.86927032  39.31740952 -74.10792542]]
% 
%>>> print knot.units[0] # Unit normal vecs.
%[ 0.79164374 -0.53951448  0.28674796]
%		\end{lstlisting}
%	\end{exampleblock}
%
%\end{frame}
%

\lecture{week 3, lecture 1}{W3L1}

\begin{frame}
\frametitle{Uncovered}
	\begin{itemize}
	\item Aliasing
	\item Transparency
	\item Gamma
	\end{itemize}
\end{frame}

\begin{frame}
	\begin{itemize}
		\item Surface normals are \alert{uniform} for each primitive when \alert{flat shading} is used.
		\item To make a surface look smooth, facets would need ot be much smaller than pixels
		\item We \emph{fake} the appearance of smooth surfaces by \alert{varying} normals across each face (\alert{Phong shading})
		\item This is like a \emph{hologram} -- the surface us still faceted but we paint it smooth. 
	\end{itemize}
	\begin{center}
	\includegraphics[width=0.25\textwidth  ]{flat-icosphere}	
	\includegraphics[width=0.25\textwidth  ]{smooth-icosphere}	
	\includegraphics[width=0.5\linewidth]{RTRFigures/RTR3_05_14}
	\end{center}
\end{frame}
\begin{frame}
	\begin{itemize}
		\item \alert{Goraud shading} evaluates the shading equation once per corner of each triangle.
		\item Graphics cards can interpolate colors quickly, bit the method has artifacts.
		\item Specular highlights \emph{must} be brightest at the corners of a facet - you can not have one in the middle of a face.
	\end{itemize}

\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{RTRFigures/RTR3_05_15}
\caption{Goraud for increasing resolutions -- the shading is always most extreme at the facet corners, which is less noticable as the resolution increases}
\label{fig:RTR3_05_15}
\end{figure}

\end{frame}
\begin{frame}
	\begin{itemize}
		\item Gouraud is the default used in OpenGL (fixed function / old days)
		\item \alert{Phong} shading moves the calculation into a fragment shader.
		\item Normals are linearly interpolated rather then colors -- allowing highlights to occur mid-facet. 
		\item Note that normals are not longer unit vectors -- must be rescaled to shade. Can be costly (worth it? probably..)
	\end{itemize}
	
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{RTRFigures/RTR3_05_16}
	\caption{Normal interpolation (Phong)}
\end{figure}
\end{frame}

\begin{frame}
	\begin{itemize}
		\item What you cannot see  in a static slide is the effect of motion.
		\item The specular highlight dances across the surface, sliding across edges from corner to corner. 
		\item It appears to add slight ripples to the pig -- you may not have noticed before but now you will see this everywhere.
		\item \alert{Takeaway:} tradeoffs are involved - changing the frequency with which you evaluate terms of the shading equation impact performance vs quality. 
	\end{itemize}
\begin{figure}
	\centering
		\includegraphics[width=1.0\linewidth]{RTRFigures/RTR3_05_17}
		\caption{Flat, Gouraud, Phong}
\end{figure}
\end{frame}

\begin{frame}{Aliasing}
	
	\begin{itemize}
		\item What is wrong with the checkerboard on the left?
		\item<2> Recall how sensors work -- they average incoming light over their surface. 
		\item<2>  If they are \emph{too} directional, then they (randomly or with a pattern) hit white or black squares.
	\end{itemize}
	\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{fig25-01a}
%\caption{}
\label{fig:fig25-01a}
\end{figure}

	
\end{frame}

\begin{frame}{Aliasing}
	\begin{itemize}
		\item This problem is related to how we sample the image plane.
		\item A related problem is \alert{jaggies} that occur when a line partially covers a pixel. It is a \alert{signal processing} issue.
		\item Lines should be invisible -- we pretend they are bands or ribbons.
	
	\end{itemize}

	\begin{center}
		\includegraphics[width=0.6\linewidth]{RTRFigures/RTR3_05_18}	
	\end{center}
\end{frame}

\begin{frame}{Temporal Aliasing}
	\begin{itemize}
		\item The same issue can occur in time (animations), called \alert{wigglies}.
		\item The classic is a spinning wheel
		\item Depending on the frame-rate, the wheel can be made to stop and even appear to spin backwards.
	\end{itemize}
	\begin{center}
		\includegraphics[width=0.7\linewidth]{RTRFigures/RTR3_05_20}\\
		Temporal aliasing
	\end{center}
\end{frame}


\begin{frame}{Sampling}
	\begin{itemize}
		\item \alert{Sampling} is necessary to represent continuous functions (like radiance values at sensors) on computers.
		\item In signal processing a \alert{signal} is represented by an array (comb) of \alert{samples} discretely spaced in \alert{time} (or space for us).
		\item To \alert{reconstruct} the continuous function translations of a continuous signal (a \alert{filter}) are added together at the samples.  
		\item Information is (usually) lost in the process.  
	\end{itemize}
	\begin{center}
		\includegraphics[width=0.7\linewidth]{RTRFigures/RTR3_05_19}\\
	    Sampling \& filtering (reconstruction)
	\end{center}
\end{frame}
\begin{frame}
	\begin{itemize}
		\item \alert{Filtering} is the process of reconstructing a sampled signal.
		\item Samples are often regularly spaced (with a certain frequency) 
		\item \alert{Aliasing} is an artifact of sampling where the frequancies of the input are distorted by the process
		\item Below, red dots are samples. depending on the sampling rate, false frequencies (\alert{aliases}) can occur in the signal.
	\end{itemize}

	\begin{center}
		\includegraphics[width=0.7\linewidth]{RTRFigures/RTR3_05_21}\\
	    Aliasing -- false frequencies
	\end{center}
\end{frame}


\begin{frame}{Nyquist Rate}
	\begin{itemize}
		\item Alasing occurs when the sampling rate is too low for the signal.
		\item The signal appears to have a lower frquency than the original.
		\item You must sample at \alert{more than twice} the original frequency
		\item This is called the \alert{sampling theorem}, the \alert{Nyquist rate} or the \alert{Nyquist rate}.
		\item \alert{Problem:} rendered scenes can have arbitrarily high frequencies.
		\item We need to "blur" (band-limit) the image before sampling to filter out higher frequencies.   This happens in the optics of most physical sensors anyhow...
	\end{itemize}
\end{frame}

\begin{frame}{Reconstruction}
	\begin{itemize}
		\item To reconstruct the original (band limted) signal we need to interpolate the discretely spaced samples.
		\item The process involves translating a continuous \alert{filter} so that it is centered at each sample.
		\item  We like filters to be zero at all but one sample -- the shape of the filter influences what we see between samples. 
	\end{itemize}
	\begin{center}
		\includegraphics[width=0.5\linewidth]{RTRFigures/RTR3_05_22}\\		{\tiny Box, tent, sync}
	\end{center}
\end{frame}


\begin{frame}
	\begin{center}
		\includegraphics[width=0.7\linewidth]{RTRFigures/RTR3_05_23}\\
		Example - box \\
		AKA nearest neighbor.
	\end{center}
\end{frame}

\begin{frame}
	\begin{center}
		\includegraphics[width=0.7\linewidth]{RTRFigures/RTR3_05_24}\\
		Example - tent\\
		AKA linear
	\end{center}
\end{frame}

\begin{frame}
	\begin{center}
		\includegraphics[width=0.7\linewidth]{RTRFigures/RTR3_05_25}\\
		Example - sync (mention wavelets?)\\
		
	\end{center}
	\begin{itemize}
		\item This is the average of a range of frequencies less than the Nyquist rate (in freq. domain). 
		\item Very much related to Gabor wavelets (computer vision, low level perception, how we see...)
	\end{itemize}
\end{frame}


\begin{frame}{Cubic}
	\begin{itemize}
		\item For some reason I did not see this in the book...
		\item Most of us have resized images before, this filter (cubic) is often used.
		\item Approximates sinc, but with finite support. 
	\end{itemize}
	
	\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{cubic-filter}
\label{fig:cubic-filter}
\end{figure}

\end{frame}

\begin{frame}
	\begin{block}{Note}
		\begin{itemize}
		\item Even when reconstruction, we still cannot draw continuous signals
		\item Reconstruction filters are usually used for \alert{resampling} for \alert{magnification}, or \alert{upsampling} images.	
		\item Useful in compression -- this is \emph{kind of} how JPEG2000 works..
		\end{itemize}
	\end{block}
\end{frame}


\begin{frame}
	\begin{center}
		\includegraphics[width=0.7\linewidth]{RTRFigures/RTR3_05_28}
		oversample
	\end{center}
\end{frame}

\begin{frame}{ }
	\begin{center}
		\includegraphics[width=0.7\linewidth]{RTRFigures/RTR3_05_29}
		oversample
	\end{center}
\end{frame}
\end{document}
import pyglet
from pyglet.gl import *
from stl.mesh import Mesh
import argparse
import shader

window = pyglet.window.Window()
fps_display = pyglet.window.FPSDisplay(window)

@window.event
def on_resize(width, height):
    if height==0: height=1
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(60., width / float(height), .1, 1000.)
    glMatrixMode(GL_MODELVIEW)
    return pyglet.event.EVENT_HANDLED

@window.event
def on_draw():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()

    glTranslatef(0, 0, -1)
    glRotatef(angle, 0, 1, 0)

    glScalef(scale, scale, scale)
    glTranslatef(*translation)

    try:
        pass
        if my_shader.linked:
            my_shader.bind()
            glDrawArrays(GL_TRIANGLES, 0, len(mesh)*3)
            my_shader.unbind()
        else:
            glDrawArrays(GL_TRIANGLES, 0, len(mesh)*3)
    except GLException as e:
        print "Dang it!"
        print e.message

    fps_display.draw()


def update(dt):
    global angle
    angle += 1

pyglet.clock.schedule_interval(update, 1/60.0)


parser = argparse.ArgumentParser()
parser.add_argument('mesh', type=argparse.FileType('rb'), help='An STL file')
parser.add_argument('--vert', type=argparse.FileType('r'), help='An vertex shader')
parser.add_argument('--frag', type=argparse.FileType('r'), help='An fragment shader')
args = parser.parse_args()
mesh  = Mesh.from_file(filename='mesh', fh=args.mesh)

if args.vert:
    vert = args.vert.read().splitlines(True)
else:
    vert=[]

if args.frag:
    frag = args.frag.read().splitlines(True)
else:
    frag=[]

glClearColor(0.5, 0.5, 0.5, 0)
glEnable(GL_DEPTH_TEST)
glEnable(GL_LIGHTING)
glEnable(GL_LIGHT0)
glEnable(GL_NORMALIZE)

glEnableClientState(GL_VERTEX_ARRAY)
vbuf = mesh.vectors.flatten()
glVertexPointer(3, GL_FLOAT, 0, vbuf.ctypes.data)

glEnableClientState(GL_NORMAL_ARRAY)
nbuf = mesh.units.repeat(3,0).flatten()
glNormalPointer(GL_FLOAT, 0, nbuf.ctypes.data)

angle = 0
center = (mesh.max_ + mesh.min_)/2
scale = 0.005
translation = -center

my_shader = shader.Shader(vert=vert, frag=frag, geom=[])

pyglet.app.run()